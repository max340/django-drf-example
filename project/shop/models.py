from django.db import models


class BaseSeo(models.Model):
    """ Base class with fields for SEO optimization"""

    seo_title = models.CharField('seo_title', max_length=500, blank=True, null=True)
    seo_description = models.CharField('seo_description', max_length=500, blank=True, null=True)
    seo_keywords = models.CharField('Keys', blank=True, null=True, max_length=500)
    seo_nofollow = models.BooleanField(
        verbose_name='No index',
        default=False,
        help_text=_("Select if it doesn't need to index")
    )

    class Meta:
        abstract = True


class Product(BaseSeo):
    """ Model for products """
    title = models.CharField('Title', max_length=255, blank=True)
    slug = models.CharField(max_length=255, unique=True, blank=True)
    price = models.DecimalField('Price', max_digits=10, decimal_places=2, blank=True, null=True)
    discount_price = models.DecimalField('Discount price', max_digits=10, decimal_places=2, blank=True, null=True)
    thumbs = models.ImageField('Thumb', upload_to='images/products', blank=True, max_length=255)
    measurement_id = models.ForeignKey('Measurement', verbose_name='Measurement', on_delete=models.SET_NULL,
                                       blank=True, null=True, related_name='product_measurement')
    is_show = models.BooleanField('Show product', default=True)
    is_sale = models.BooleanField('Sale', default=False)
    show_in_main_page = models.BooleanField('Show product in main page', default=False)
    order_in_main_page = models.IntegerField('Product order in main page', default=1, null=True, blank=True)
    order = models.IntegerField('Order', null=True)
    video_feedback = models.ManyToManyField('VideoFeedback', blank=True, related_name='product')
    category = models.ManyToManyField('Category', verbose_name="Category", blank=True, related_name='product_category')
    main_category = models.ForeignKey('Category', verbose_name="Main category", blank=True, null=True,
                                      on_delete=models.SET_NULL, related_name='main_product_category')
    relation_products = models.ManyToManyField('Product', verbose_name="Related product", blank=True,
                                               related_name='relation_product')
    created_at = models.DateTimeField('Created date', auto_now_add=True)
    last_modified = models.DateTimeField('Updated date', auto_now=True)

    class Meta:
        verbose_name = 'Product'
        verbose_name_plural = 'Products'

    def __str__(self) -> str:
        return self.title

    def get_thumb(self) -> str | None:
        """ Generate thumb path """

        if self.thumbs:
            return settings.URL + settings.MEDIA_URL + str(self.thumbs)

    def get_measurement_name(self) -> str | None:
        """ Get measurement name  """
        if self.measurement_id:
            return self.measurement_id.name

    def is_variation(self) -> bool:
        """ Check product is variation or not"""
        return True if self.product_variation.count() else False

    def get_url(self) -> str | None :
        """ Get product url """
        if self.main_category:
            get_category_url = self.main_category.get_url()
            if get_category_url:
                return get_category_url + self.slug + '/'


class Category(BaseSeo):
    """ Model for products category  """
    title = models.CharField('Category', max_length=255, blank=True)
    slug = models.CharField(max_length=255, unique=True, blank=True)
    icon = models.ImageField('Icon', upload_to='category_icons/', blank=True)
    parent = models.ForeignKey(
        'self',
        on_delete=models.CASCADE,
        verbose_name='Parent category',
        related_name='parent_category',
        blank=True,
        null=True
    )
    order = models.IntegerField('Order', null=True, default=1)
    thumb = models.ImageField('Logo', upload_to='thumbs', max_length=255, blank=True)

    def get_thumb(self) -> str | None:
        """ Generate thumb path """
        if self.thumb:
            return settings.URL + settings.MEDIA_URL + str(self.thumb)

    def get_icon(self) -> str | None:
        """ Generate icon path """
        if self.icon:
            return settings.URL + settings.MEDIA_URL + str(self.icon)

    def __str__(self) -> str:
        return self.title

    class Meta:
        verbose_name = 'Product category'
        verbose_name_plural = 'Product category'


class Measurement(models.Model):
    """ Model for products measurements """
    name = models.CharField('Title', max_length=255, blank=True)
    slug = models.CharField(max_length=255, unique=True, blank=True)

    def __str__(self) -> str:
        return self.name

    class Meta:
        verbose_name = 'Measurement'
        verbose_name_plural = 'Measurement'