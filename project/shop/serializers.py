from shop.models import Order, Product, Parameter

logger = logging.getLogger('shop')


class OrderSerializer(DynamicFieldsModelSerializer):
    """ Serialize order data """
    order_cart = serializers.SerializerMethodField('get_cart', required=False)
    total = serializers.DecimalField(max_digits=10, decimal_places=2, required=False)
    filial_email = serializers.CharField(source="filial_id.email", required=False)
    filial_title = serializers.CharField(source="filial_id.title", required=False)
    payment_id = serializers.SerializerMethodField('get_payment', required=False)
    cart_count = serializers.SerializerMethodField()

    @staticmethod
    def get_cart_count(obj) -> int:
        """ Get cart count """
        return obj.order_cart.count()

    @staticmethod
    def get_payment(order: Order) -> dict:
        """ Get payment serialize data """
        queryset = Payment.objects.filter(payment_related=order).first()
        serializer = PaymentSerializer(queryset, many=False)
        return serializer.data

    def get_cart(self, order_id: int) -> dict:
        """ Get cart serialize data """
        queryset = Cart.objects.filter(order_id=order_id, product_id__is_show=True) \
            .exclude(variation_id__is_show=False).all()
        serializer = CartSerializer(queryset, many=True, context=self.context)
        return serializer.data

    class Meta:
        model = Order
        fields = '__all__'


class CartSerializer(serializers.ModelSerializer):
    """ Serialize cart data """
    parameters = serializers.CharField(required=False)
    id = serializers.CharField(required=False)
    cart_parameters = CartParametersSerializer(many=True, required=False)
    total_price = serializers.DecimalField(max_digits=10, decimal_places=2, required=False)
    product_title = serializers.CharField(source='product_id.title', required=False)
    product_thumb = serializers.CharField(source='product_id.get_thumb', required=False)
    product_url = serializers.CharField(source='product_id.get_url', required=False)
    attributes = serializers.SerializerMethodField('get_variation_attribute')
    current_price = serializers.SerializerMethodField('get_price')

    def get_price(self, cart: Cart) -> float:
        """ Retrieve current price for product """
        product_type = 'product_id'
        role = 'productrole_set'

        if self.context.get('user_role'):
            for role_item in getattr(getattr(cart, product_type), role).all():
                if role_item.role_id.pk == self.context.get('user_role').pk:
                    return role_item.discount_price or role_item.price
        else:
            get_product_id = getattr(cart, product_type)
            return get_product_id.discount_price or get_product_id.price

    @staticmethod
    def get_variation_attribute(cart) -> dict | None:
        """ Retrieve variation attributes """
        if cart.variation_id:
            serializer = ProductAttributeValueSerializer(cart.variation_id.attributes, many=True)
            return serializer.data

    @staticmethod
    def _validate_order(order_item: Order) -> bool:
        """ Validate order status """
        if order_item:
            if order_item.status != 'collected':
                return True
        raise Order.DoesNotExist

    def _get_or_create_order(self, order_item: Order | None) -> Order:
        """ Get order or create new item """
        if order_item:
            self._validate_order(order_item)
        else:
            order_item = Order.objects.create()
        return order_item

    @staticmethod
    def _search_product_in_cart(order_id: int, product_id: int, price: float, variation_id: int | None = None,
                                parameters: str = None) -> Cart:
        """ Search product in cart """
        cart = Cart.objects.filter(order_id=order_id, price=price, product_id=product_id)

        if variation_id:
            cart = cart.filter(variation_id=validated_data['variation_id'])
        if parameters:
            try:
                for item in json.loads(validated_data.get('parameters')):
                    cart = cart.filter(cart_parameters__parameter_id=item['parameter_id'],
                                       cart_parameters__value=item['value'])
            except ValueError:
                logger.debug('Add to cart: JSON loads error: ' + validated_data['parameters'])
        return cart

    @staticmethod
    def _increase_quantity_for_product(cart: Cart, quantity: int) -> None:
        """ Add quantity to product """
        cart = cart.get()
        cart.quantity = cart.quantity + quantity
        cart.save()

    @transaction.atomic()
    def _create_new_cart(self, order: Order, product_data: dict) -> Cart:
        """ Create new cart item """
        sid = transaction.savepoint()
        cart_data = {
            'price': product_data.get('price'),
            'order_id': order,
            'quantity': product_data.get('quantity'),
            'product_id': product_data.get('product')
        }
        if 'variation_id' in product_data:
            cart_data['variation_id'] = product_data.get('variation_id')
        cart = Cart.objects.create(**cart_data)

        if 'parameters' in product_data:
            try:
                for parameter in json.loads(validated_data['parameters']):
                    get_parameter = product_data.get('product').product_parameters.get(pk=parameter['parameter_id'])
                    cart.cart_parameters.create(parameter_id=get_parameter, value=parameter['value'])
            except ValueError:
                transaction.savepoint_rollback(sid)
                logger.debug('JSON load error: ' + validated_data['parameters'])
                raise serializers.ValidationError({'code': 400, 'error': 'Ошибка сервера'})

        transaction.savepoint_commit(sid)
        return cart

    def create(self, validated_data: dict) -> Cart:
        """Add product to cart"""
        order_item = self._get_order(validated_data.get('order'))
        cart = self._search_product_in_cart(order_id=order_item.pk, product_id=validated_data.get('product_id'),
                                            price=validated_data.get('price'),
                                            variation_id=validated_data.get('variation_id'),
                                            parameters=validated_data.get('parameters'))

        if cart:
            return self._increase_quantity_for_product(cart=cart, quantity=validated_data.get('quantity', 0))
        else:
            return self._create_new_cart()

    class Meta:
        model = Cart
        fields = '__all__'
        extra_kwargs = {'product_id': {'required': True}, 'quantity': {'required': True}, 'price': {'required': True}}
